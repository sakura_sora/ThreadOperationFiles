package pers.sakurasora.threadoperationfiles;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import pers.sakurasora.threadoperationfiles.util.QueueArray;

/**
 * @author SakuraSora
 * @Email 1468071675@qq.com
 * @DateTime 2019年2月16日 下午2:58:35
 *
 * @Description
 *				多线程--文件拆分合并<br>
 *				要求：<br>
 *					1. 几个文件，几个线程<br>
 *					2. 读取 第1个文件第一行,第2个文件第一行,...第n个文件第一行;	写入文件;<br>
 *					3. 读取 第1个文件第二行,第2个文件第二行,...第n个文件第二行;	写入文件;<br>
 *					......
 *					4. 读取 第1个文件第n行,第2个文件第n行,...第n个文件第n行;	写入文件;<br>
 *					5. 直到所有文件读取写入完毕<br>
 *
 *				注：此程序文件把写入文件替换为输出到控制台
 */
public class FileThread implements Runnable {
	/**
	 * 锁对象
	 */
	private static Object lock = new Object();
	/**
	 * 循环列表<br>
	 * 控制文件读取顺序
	 */
	private static QueueArray sequence;
	/**
	 * 访问循环列表的当前下标<br>
	 * 默认初始值为0
	 */
	private static int iCurrentIndex;
	/**
	 * 要操作文件数量<br>
	 * 值为0时跳出循环
	 */
	private static int iFileNums;
	/**
	 * BufferedReader对象，用于读取文件
	 */
	private BufferedReader brReader; 
	/**
	 * 承接所读取的结果<br>
	 * 默认初始值为null
	 */
	private String line;

	/**
	 * 有参构造函数：打开要读取的文件
	 * 
	 * @param sPathName 文件路径
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	public FileThread(String sPathName) 
			throws UnsupportedEncodingException, FileNotFoundException {
		File file = new File(sPathName);
		InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "GBK"); // 解决读取结果中文乱码问题
		this.brReader = new BufferedReader(isr);
	}

	@Override
	public void run() {
		synchronized (lock) {
			try {
				while (iFileNums > 0) {
					if (sequence.getValueOfIndex(iCurrentIndex) != Integer.parseInt(Thread.currentThread().getName())) {
						lock.wait();    // 当前线程等待
					} else {
						line = this.brReader.readLine();    // 读取文本文件
						iCurrentIndex++;
						if (line == null) {
							this.brReader.close();
							iFileNums--;
							sequence.removeByValue(Integer.valueOf(Thread.currentThread().getName()));
							iCurrentIndex--;
							lock.notifyAll();    // 唤醒其他所有线程
							Thread.currentThread().stop();    // 停止当前线程
						} else {
							System.out.println(line);    // 读一行就列出一行
							lock.notifyAll();    // 唤醒其他所有线程
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 获取访问循环列表当前下标
	 * 
	 * @return 当前下标
	 */
	public static int getiCurrentIndex() {
		return iCurrentIndex;
	}
	
	/**
	 * 设置访问循环列表的当前下标
	 * 
	 * @param iCurrentIndex
	 */
	public static void setiCurrentIndex(int iCurrentIndex) {
		FileThread.iCurrentIndex = iCurrentIndex;
	}

	/**
	 * 获取文件数量
	 * 
	 * @return 文件数量
	 */
	public static int getiFileNums() {
		return iFileNums;
	}

	/**
	 * 设置文件数量<br>
	 * 根据文件数量初始化循环列表
	 * 
	 * @param iFileNums 文件数量
	 */
	public static void setiFileNums(int iFileNums) {
		FileThread.iFileNums = iFileNums;
		sequence = new QueueArray(iFileNums);
	}

}
