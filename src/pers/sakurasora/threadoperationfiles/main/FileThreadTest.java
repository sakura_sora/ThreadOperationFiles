package pers.sakurasora.threadoperationfiles.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import pers.sakurasora.threadoperationfiles.FileThread;

/**
 * @author SakuraSora
 * @Email 1468071675@qq.com
 * @DateTime 2019年2月16日 下午2:58:35
 *
 * @Description
 *				
 */
public class FileThreadTest {
	/**
	 * 指定目录
	 */
	private static File dir;
	/**
	 * 存放指定目录下所有文件名的数组
	 */
	private static String[] sDirContents;
	/**
	 * 线程数组
	 */
	private static Thread[] threads;

	public static void main(String[] args) {
		dir = new File("ThreadOperationFiles");
//		dir.mkdir();
		if (!dir.exists()) {
			System.out.println("指定目录不存在！");
		}
		/**
		 * 列出目录下的所有文件
		 * 如果当前目录没有文件，给出提示
		 * 否则，启动线程
		 */
		if (dir.isDirectory()) {
			sDirContents = dir.list();
			if (sDirContents.length <= 0) {
				System.out.println("当前目录没有文件！");
			} else {
				FileThread.setiFileNums(sDirContents.length);
				threads = new Thread[sDirContents.length];
				/**
				 * 几个文件，几个线程
				 */
				for (int i = 0; i < sDirContents.length; i++) {
					try {
						threads[i] = new Thread(new FileThread(dir.getName()+"/"+sDirContents[i]), String.valueOf(i+1));
						threads[i].start();
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

}
