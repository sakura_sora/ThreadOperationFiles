package pers.sakurasora.threadoperationfiles.util;

import java.util.ArrayList;
import java.util.List;

import pers.sakurasora.threadoperationfiles.FileThread;

/**
 * @author SakuraSora
 * @Email 1468071675@qq.com
 * @DateTime 2019年2月16日 下午7:26:32
 *
 * @Description
 *				工具类--模拟循环队列：循环列表（存放整型数值）
 */
public class QueueArray {
	/**
	 * 存放1、2、...n的列表
	 */
	private static List<Integer> queueLst = new ArrayList<Integer>();
	
	/**
	 * 有参构造函数：初始化列表
	 * 
	 * @param iLength 列表长度
	 */
	public QueueArray(int iSize) {
		for (int i = 1; i <= iSize;i++) {
			queueLst.add(i);
		}
	}
	
	/**
	 * 获取下标为index的值
	 * 
	 * @param index 下标
	 * @return 循环列表下标为index的值
	 */
	public int getValueOfIndex(int index) {
		/**
		 * 如果index >= length-->index = 0;
		 * 并修改FileThread.iCurrentIndex的值为0
		 */
		if (index >= queueLst.size()) {
			index = 0;
			FileThread.setiCurrentIndex(index);
		}
		return queueLst.get(index);
	}
	/**
	 * 移除列表中指定的整型值
	 * 
	 * @param iValue 指定的整型值
	 */
	public void removeByValue(int iValue) {
		queueLst.remove(new Integer(iValue));
	}
	
}
